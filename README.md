To run the program:

1. Download the jar file `ClarifaiPOC.jar`
2. Set the environment variable `CLARIFAI_API_KEY` with your API key
3. Use Java version 1.8
4. Run the jar file using the following command:
```
java -cp <path to ClarifaiPOC.jar> ClarifaiPOC
```
