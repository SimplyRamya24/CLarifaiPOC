import clarifai2.api.ClarifaiBuilder;
import clarifai2.api.ClarifaiClient;
import clarifai2.api.request.model.PredictRequest;
import clarifai2.dto.input.ClarifaiInput;
import clarifai2.dto.input.ClarifaiURLImage;
import clarifai2.dto.model.Model;
import clarifai2.dto.model.output.ClarifaiOutput;
import clarifai2.dto.prediction.Concept;
import clarifai2.exception.ClarifaiException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;

class FileTagger {

    private String apiKey = "e47f6d557d6f4685bc3e0c11328afe4c";
    private final ClarifaiClient client = new ClarifaiBuilder(apiKey).buildSync();
    private ReadImageInput readImageInput = new ReadImageInput();
    private Model<Concept> generalModel = client.getDefaultModels().generalModel();

    FileTagger() {
        String apiKeyEnv = System.getenv("CLARIFAI_API_KEY");
        if (apiKeyEnv != null){
            apiKey = apiKeyEnv;
        }else{
            System.err.println("CLARIFAI_API_KEY not set. Using default key.");
        }
    }

    HashMap<String, PriorityQueue<TagResult>> getTagStore() {
        return tagStore;
    }

    private HashMap<String, PriorityQueue<TagResult>> tagStore = new HashMap<>();


    void startTaggingAsync(OnCompletion completionCallback) {
        final ArrayList<String> imageLines = readImageInput.getImageLines();

        ArrayList<PredictRequest<Concept>> requestList = new ArrayList<>();
        int requestIndex = 0;
        ArrayList<ClarifaiInput> inputList = new ArrayList<>();
        for (String imageURL : imageLines) {
            inputList.add(ClarifaiInput.forImage(imageURL));
            if (requestIndex == 65) { //starting at half the capacity to "see" progress
                requestIndex = 0;
                PredictRequest<Concept> request = generalModel.predict().withInputs(
                        inputList
                );
                requestList.add(request);
                inputList = new ArrayList<>();
            } else {
                requestIndex++;
            }
        }
        if (inputList.size() > 0) {
            PredictRequest<Concept> requestResidual = generalModel.predict().withInputs(
                    inputList
            );
            requestList.add(requestResidual);
        }
        int index = 0;
        int totalRequests = requestList.size();
        final int[] requestProcessed = new int[totalRequests];

        for (PredictRequest<Concept> aRequestList : requestList) {
            try {
                TimeUnit.SECONDS.sleep(1); //free plan throws 429
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            final int lambdaIndex = index;
            requestProcessed[lambdaIndex] = 0;
            System.out.println("Started request " + (lambdaIndex + 1) + "...");
            aRequestList.executeAsync(
                    (List<ClarifaiOutput<Concept>> outputs) -> {
                        requestProcessed[lambdaIndex]=1;
                        for (ClarifaiOutput<Concept> op : outputs) {
                            appendToTagStore(op);
                        }
                        System.out.println("Finished request " + (lambdaIndex + 1) + "...");
                        if (isAllRequestsProcessed(requestProcessed)) {
                            completionCallback.onTaggingCompletion();
                        }
                    },
                    (int code) -> {
                        requestProcessed[lambdaIndex]=1;
                        System.err.println("Error code: " + code);
                        if (isAllRequestsProcessed(requestProcessed)) {
                            completionCallback.onTaggingCompletion();
                        }
                    },
                    (IOException e) -> {
                        requestProcessed[lambdaIndex]=1;
                        if (isAllRequestsProcessed(requestProcessed)) {
                            completionCallback.onTaggingCompletion();
                        }
                        throw new ClarifaiException(e);
                    }
            );
            index++;
        }


    }
    private boolean isAllRequestsProcessed(int[] requests){
        for (int request : requests) {
            if (request == 0) {
                return false;
            }
        }
        return true;
    }
    public interface OnCompletion {
        void onTaggingCompletion();
    }

    private void appendToTagStore(ClarifaiOutput<Concept> op) {
        //Can assert class here
        ClarifaiURLImage clarifaiURLImage = (ClarifaiURLImage) op.input().inputValue();
        String url = clarifaiURLImage.url().toString();
        for (Concept concept : op.data()) {
            String tagName = concept.name();
            TagResult tagResult = new TagResult(concept.value(), url);
            if (tagStore.get(tagName) == null) {
                PriorityQueue<TagResult> zeroTagResults = new PriorityQueue<>(
                        11,
                        new TagResultComparator()
                );
                tagStore.put(tagName, zeroTagResults);
            }
            PriorityQueue<TagResult> tagResults = tagStore.get(tagName);
            tagResults.add(tagResult);
            if (tagResults.size() > 10) {
                tagResults.poll();//keep removing the minimum
            }
        }
    }

    class TagResult {
        private float rank;
        private String url;

        TagResult(float rank, String url) {
            this.rank = rank;
            this.url = url;

        }

        @Override
        public String toString() {
            return "Tag :" + this.url + " " + this.rank;
        }
    }

    class TagResultComparator implements Comparator<TagResult> {

        @Override
        public int compare(TagResult o1, TagResult o2) {
            return Float.compare(o1.rank, o2.rank);
        }


    }
}
