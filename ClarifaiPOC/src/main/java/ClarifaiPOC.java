import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Scanner;

public class ClarifaiPOC {
    public static void main(String[] args) {

        System.out.println("Starting tagging...");
        FileTagger fileTagger = new FileTagger();
        HashMap<String, PriorityQueue<FileTagger.TagResult>> store = fileTagger.getTagStore();
        final String prompt = "Enter tag (or :exit to quit):";
        System.out.println("Waiting  for tagging to finish..");

        fileTagger.startTaggingAsync(
                //This callback gets called once tagging is done
                () -> {
                    synchronized (store){
                        System.out.println("Tagging completed. \n"+prompt);
                        store.notifyAll();
                    }
                }
        );

        try {
            synchronized (store) {
                store.wait();
                String inTag;
                Scanner sc = new Scanner(System.in);
                while(!(inTag = sc.nextLine()).equals(":exit")){
                    PriorityQueue<FileTagger.TagResult> resultQ = store.get(inTag);
                    if (resultQ == null) {
                        System.out.println("Unable to find tag.\n"+prompt);
                    }else {
                        //switch to reverse order while printing
                        PriorityQueue<FileTagger.TagResult> copy = new PriorityQueue<>(
                                resultQ.size(), resultQ.comparator().reversed()
                        );
                        copy.addAll(resultQ);
                        for(int i=0;i <10;i++){
                            if (copy.peek() == null){
                                System.out.println("No more images for this tag");
                                break;
                            }
                            System.out.println(copy.poll());
                        }
                        System.out.println(prompt);
                    }
                }

        }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Forcefully exit without waiting for async threads to exit
        System.exit(0);

    }
}
