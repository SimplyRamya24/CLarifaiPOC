import java.io.*;
import java.util.ArrayList;

class ReadImageInput {

    ArrayList<String>  getImageLines() {
        final String FILENAME = "images.txt";
        InputStream file = getClass().getClassLoader().getResourceAsStream(FILENAME);
        BufferedReader br = null;
        InputStreamReader fr = null;
        ArrayList<String> inputURLs = new ArrayList<>();
        try {
            fr = new InputStreamReader(file);
            br = new BufferedReader(fr);
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                inputURLs.add(sCurrentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return inputURLs;
    }
}
